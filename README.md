# SMS over Mail

## Goal
Send SMS from mail
Receive SMS on mail

## Use cases

### Send SMS over Mail

* User connects to their mail address (registered to the app) and send mail to the app mail address (set by user) with a special subject : `[APPLICATIONNAME/Send:NUMBEROFTHESMSRECEIVER] Blablabla` (the app doesn't read the Blabla)
* User's app' check mail (every 5 or 10s normally and every 1s if in the middle of a conversation)
* If there's a mail following the standard (subject starting with : `[APPLICATIONNAME/Send:NUMBEROFTHESMSRECEIVER]` + from a whitelisted mail address + Unread) :
    * Send an SMS to `NUMBEROFTHESMSRECEIVER`
    * Mark it as read

### Receive SMS over Mail

* User receive an SMS
* The app automaticly send a mail to the User's mail address with subject `[APPLICATIONNAME/Received:NUMBER] <NameOfContact>`

### Config

* Set app's mail address to check in for SMS to send + to send mail to User's personnal mail address
* Set users's mail address(es) to send received SMS [Advanced-Config] (same address as app's one is [easy-config])
* Set mail addresses allowed to send mail to app's mail address [Advanced-Config] (same as app's one in [easy-config])
* Set Subject standard (has to contain `NUMBEROFTHESMSRECEIVER` in it) [Advanced-Config]
* GPG Public Keys [Advanced-Config]

## More to know

Version 1 futures needed to get out of Alpha :

* The app will be able to use encryption and is compatible with Silence App (for send/receive SMS)
* The app can use GPG for mails.
* The app can connect to GPG Keys servers to automaticaly get User's keys.
* The app has an Easy-Config and an Advanced-Config
* The app check MX config
* The app has basic securities for mail exchanges (IMAP, SMTP,…)